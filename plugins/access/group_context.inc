<?php
/**
 * @file
 * Plugin to check if the title has configured string.
 */

$plugin = array(
  'title' => t('Group Context'),
  'description' => t('Checks if group context exists or not.'),
  'callback' => 'group_context_ctools_access_check',
  'default' => array('negate' => 0),
  'summary' => 'group_context_ctools_access_summary',
//  'settings form' => 'group_context_ctools_access_settings',
//  'required context' => new ctools_context_required(t('Group'), 'group'),
);


function group_context_ctools_access_check($conf, $context) {
  $group_context = group_context();
  if (!$group_context || $group_context['group_type'] != 'group') {
    return FALSE;
  }

  return TRUE;
}


/**
 * Provide a summary description.
 */
function group_context_ctools_access_summary($conf, $context) {
 
  return t('Checks if either group context exists or not');
}